onload = function(){
  //Cambio el h2 porque soy así:
  var miH = document.getElementByTagName("h2");
  miH.innerHTML = "Púlsame para ver los puntos de ruptura del DOM"

  miH.onclick = function(){
    //Cambio el color de h2
    miH.style.color = "purple";

    //Creo un párrafo nuevo porque puedo:
    var miP = document.createElement("p");
    miP.innerHTML = "Esta es mi creación!";
    miP.style.fontSize = "24px";
  }
}
