function mostrarPrecio(){
  var productos = generaSurtido();
  //Abrimos la tabla y la fila de la tabla
  document.write("<table><tr>");
  for (var i=0; i < productos.length; i++) {
    //Espaciamos los elementos 80px para que aparezcan debajo de su imagen
    document.write("<td width='80px'><h2>" + consultaPrecios(i) + "</h2></td>");
  }
  //Cerramos la tabla y la fila de la tabla
  document.write("</tr></table>");
}

// function mostrarSurtido(){
//   var productos = generaSurtido();
//   /*Generamos el array de fotos con los nombres de los ficheros.
//   En este caso podemos usar un array porque los ficheros se llaman como los nombres
//   Si no fuera así, tendríamos que cargar uno a uno los nombres.
//   Ej: foto[0] = "foto1234.jpg"*/
//   var fotos = [];
//   for (var i=0; i < productos.length; i++) {
//     fotos[i] = productos[i] + ".jpg";
//   }
//   //console.log(productos);
//   for (var i=0; i < productos.length; i++) {
//     document.write("<img src=" + fotos[i] + " width='80px' onclick='mostrarPrecio()'></img>");
//   }
// }
