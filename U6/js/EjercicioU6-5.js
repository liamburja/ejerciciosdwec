onload = function(){
  var n = Math.floor(Math.random()* (1000-10000))+10000;
  console.log(n);
  document.getElementById('ncap').innerHTML = n;
  var resul="";
  var ret=false;
  formulario.onsubmit=function(){
    //Comprobamos el nombre
    if (formulario.nombre.value == null || formulario.nombre.length == 0 || !(/^\S+$/.test(formulario.nombre.value))) {
          resul += "El nombre introducido no es correcto <br/>";
          ret = false;
    }else{
      resul += "Nombre Correcto <br/>";
      ret = true;
    }
    //Comprobamos los apellidos
    if (formulario.apellidos.value == null || formulario.apellidos.length == 0 || !(/^\S+$/.test(formulario.apellidos.value))) {
          resul += "El apellido introducido no es correcto <br/>";
          ret = false;
    }else{
      resul += "Apellidos Correctos <br/>";
      ret = true;
    }
    //Comprobamos que el nombre y los apellidos no sean iguales
    if (formulario.nombre.value == formulario.apellidos.value) {
      resul += "El nombre y los apellidos son lo mismo<br/>";
      ret = false;
    }else {
      resul += "El nombre y los apellidos no son iguales<br/>";
      ret = true;
    }
    //Comprobamos la edad
    if (formulario.edad.value == null || formulario.edad.length == 0 || !(/^\w+$/.test(formulario.apellidos.value))) {
      resul += "La edad esta mal introducido<br/>";
      ret = false;
    }else if(formulario.edad.value >= 18){
      resul += "Es mayor de 18 años<br/>";
      ret = true;
    }else {
      alert("Es menor de 18 años");
      ret = false;
    }
    //Comprobamos el email
    if (formulario.email.value == null || formulario.email.length == 0) {
      resul += "El email esta mal introducido<br/>";
      ret = false;
    }else {
      resul += "Email correctamente introducido<br/>";
      ret=true;
    }
    //Comprobamos el DNI
      if (!(/^\d{8}[a-z]$/.test(formulario.dni.value))) {
        resul += "DNI no introducido o mal introducido<br/>";
        ret=false;
      }else {
        resul += "DNI correctamente introducido<br/>";
        ret=true;
      }
    //Comprobamos el checked
    if (formulario.condiciones.checked){
      resul += "Las condiciones estan aceptadas<br/>";
      ret=true;
    }else {
      resul += "Las condiciones no estan aceptadas<br/>";
      ret=false;
    }
    //Comprobamos el captcha
    if (formulario.captcha.value == n || formulario.captcha.length != 0) {
      resul += "Captcha correctamente introducido<br/>";
      ret = true;
    }else {
      resul += "Captcha incorrectamente introducido<br/>";
      ret=false;
    }

    if (ret) {
      resultado.style.color = "green";
    }else {
      resultado.style.color = "red";
    }
    resultado.innerHTML = resul;
    return ret;
  }
}
