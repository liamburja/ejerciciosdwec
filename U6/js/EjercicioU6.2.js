onload = function(){
var formulario = document.forms[0];
formulario.onsubmit = function(){
    console.log("He entrado");
    var err = document.getElementsByClassName('errores')[0];
    var errList ="";
    var ret= true;
    //Nombre
    var nombre = formulario.nombre.value;
    errList += "Nombre: " + nombre + "<br/>";
    ret = false;

    //Apellidos:
    var apellidos = formulario.apellidos.value;
    errList += "Apellidos: " + apellidos + "<br/>";
    ret = false;

    //validamos el radiobutton
    var genero = formulario.sexo.value;

    errList += "Genero: " + genero + "<br/>";
    ret = false;

    //Fecha
    var fecha = (document.getElementsByName('fecha')[0].value).split('-');
    var ano=fecha[0];
    var mes=fecha[1];// de 0 a 11
    var dia=fecha[2];// 1 a 31
    var nf= new Date(ano,(mes - 1),dia);
    errList += "Fecha: " + nf + "<br/>";
    ret = false;

    var hoy = new Date();
    //resto los años de las dos fechas
   	var edad = hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año
   	//si resto los meses y me da mayor que 0, ha cumplido años
   	if (hoy.getMonth() + 1 - mes > 0){
      	edad++;
    }
   	//si resto los dias y me da menor que 0 entonces no ha cumplido años.
    //si da mayor o igual si ha cumplido
   	if (hoy.getUTCDate() - dia >= 0){
      	edad++;
    }
    errList += "Edad: " + edad + "<br/>";
    ret = false;

    //Telefono
    var tel = document.getElementsByName('telefono')[0].value;
    errList +="Telefono: " + tel + "<br/>";
    ret = false;

    //seleccion
    var prov = document.getElementsByName("provincia")[0].selectedIndex;
    errList += "Provincia: " + prov + "<br/>";
    ret = false;

    //Email
    var email = document.getElementsByName("email")[0].value;
    errList += "Email: " + email + "<br/>";
    ret = false;

    var emailRepe = document.getElementById("emailRepe").value;
    errList += "Email repetido: " + emailRepe + "<br/>";
    ret = false;

    if (ret){
      errList = "Tu petición se ha enviado correctamente";
      err.style.color="blue";
    }else{
      err.style.color="black";
    }

    err.innerHTML = errList;
    return ret;


}
}
