window.onload = function(){

  formulario.onsubmit = function(){
    //localStorage
    if(localStorage != undefined){
    var nombre = formulario.nombre.value;
    localStorage.setItem("nombre", nombre);
    var apellidos = formulario.apellidos.value;
    localStorage.setItem("apellidos", apellidos);

    var sexo=document.getElementsByName("sexo");
        for(var i=0;i<sexo.length;i++)
        {
            if(sexo[i].checked)
              sexo = sexo[i].value;
              localStorage.setItem("sexo",sexo);
        }
}else {
  document.getElementsByClassName('errores')[0].innerHTML = "Tu navegador no soporta localStorage";
}
  if (sessionStorage != undefined) {


    //sessionStorage
    var color = formulario.color.value;
    sessionStorage.setItem("color", color);
    var provincia = formulario.selector.value;
    sessionStorage.setItem("provincia", provincia);
    var poblacion = formulario.poblacion.value;
    sessionStorage.setItem("poblacion", poblacion);
}else {
  document.getElementsByClassName('errores')[0].innerHTML = "Tu navegador no soporta sessionStorage";
}
  if (formulario.foro.checked == true) {
      formulario.action = "EjercicioU6-3Foro.html";
  }
  else if (formulario.eventos.checked == true) {
      formulario.action = "EjercicioU6-3Evento.html";
  }
}
}
